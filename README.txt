Podporovane use cases:

- Create project
- Change project
- Create task
- Update task
- Assign task
- Start task
- Resolve task
- Close task
- Comment task
- Plan sprint