from django import forms
from django.contrib.auth.models import User

from django.forms.models import modelform_factory
from django.shortcuts import render, redirect, resolve_url

from project.application import Application, DjangoRepository
from project.models import Project, Task


ProjectForm = modelform_factory(Project, fields=['shortcut', 'name'])

app = Application(DjangoRepository())

# Create your views here.
def create_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)

        if form.is_valid():
            project_id = app.project_service.create_project(**form.cleaned_data)
            return redirect(resolve_url('project.detail', project_id))
    else:
        form = ProjectForm()

    return render(request, 'project/project_create.html', {'form': form})


def project_detail(request, project_id):
    project = app.project_service.read_project(project_id)

    form = ProjectForm(instance=project)
    if request.method == 'POST':
        form = ProjectForm(request.POST, instance=project)

        if form.is_valid():
            app.project_service.update_project(project_id, **form.cleaned_data)
            return redirect(resolve_url('project.detail', project_id))

    return render(request, 'project/project_detail.html', {'form': form})


def project_list(request):
    projects = app.project_service.list_projects()
    return render(request, 'project/project_list.html', {'projects': projects})


TaskForm = modelform_factory(Task, fields=['name', 'description'])
AssignUserForm = modelform_factory(Task, fields=['assigned_user'])

def task_create(request, project_id):
    form = TaskForm()

    if request.method == 'POST':
        form = TaskForm(request.POST)

        if form.is_valid():
            task_id = app.task_service.create_task(
                project_id, **form.cleaned_data
            )

            return redirect(resolve_url('task.detail', task_id))

    return render(request, 'task/task_create.html', {'form': form})


def task_detail(request, task_id):
    task = app.task_service._read_task(task_id)


    form = TaskForm(instance=task)
    assigned_form = AssignUserForm(instance=task)
    comments = app.task_service.list_task_comments(task_id)
    comment_form = CommentTaskForm()

    return render(request, 'task/task_detail.html', {
        'task': task,
        'form': form,
        'assigned_form': assigned_form,
        'comments': comments,
        'comment_form': comment_form
    })


def assign_user(request, task_id):
    task = app.task_service._read_task(task_id)

    if request.method == 'POST':
        form = AssignUserForm(request.POST, instance=task)

        if form.is_valid():
            app.task_service.assign_task(task_id, form.cleaned_data['assigned_user'].pk)

    return redirect(resolve_url('task.detail', task_id))

def start_task(request, task_id):
    app.task_service.start_task(task_id)
    return redirect(resolve_url('task.detail', task_id))

class ResolveTaskForm(forms.Form):
    resolution = forms.CharField(widget=forms.Textarea())

def resolve_task(request, task_id):
    form = ResolveTaskForm()

    if request.method == 'POST':
        form = ResolveTaskForm(request.POST)
        if form.is_valid():
            app.task_service.resolve_task(task_id, form.cleaned_data['resolution'])
            return redirect(resolve_url('task.detail', task_id))

    return render(request, 'task/task_resolve.html', {'form': form})

def close_task(request, task_id):
    app.task_service.close_task(task_id)
    return redirect(resolve_url('task.detail', task_id))

class CommentTaskForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea())

def comment_task(request, task_id):
    form = CommentTaskForm()

    if request.method == 'POST':
        form = CommentTaskForm(request.POST)
        if form.is_valid():
            app.task_service.comment_task(task_id, request.user.pk, form.cleaned_data['message'])

    return redirect(resolve_url('task.detail', task_id))

def task_list(request, project_id):
    tasks = app.task_service.list_project_tasks(project_id)

    return render(request, 'task/task_list.html', {
        'tasks': tasks
    })