from datetime import datetime
from project.models import Project, Task, Comment

NONE = object()

class Application(object):
    def __init__(self, repository):
        self.project_service = ProjectService(repository=repository)
        self.task_service = TaskService(repository=repository, clock=datetime)

class ProjectService(object):
    def __init__(self, repository):
        self._repository = repository

    def create_project(self, shortcut, name):
        project = Project(shortcut=shortcut, name=name)

        project_id = self._repository.save(project)

        return project_id

    def _read_project(self, project_id):
        return self._repository.read_project(project_id)

    def update_project(self, project_id, shortcut=NONE, name=NONE):
        project = self._read_project(project_id)

        if shortcut is not NONE:
            project.change_shortcut(shortcut)

        if name is not None:
            project.change_name(name)

        self._repository.save(project)

    def plan_sprint(self, project_id, start, end, tasks):
        project = self._read_project(project_id)

        tasks = self._repository.tasks_by_ids(tasks)
        sprint = project.plan_sprint(start, end, tasks)

        return self._repository.save(sprint)

    def read_project(self, project_id):
        return self._read_project(project_id)

    def list_projects(self):
        return self._repository.list_projects()


class TaskService(object):
    def __init__(self, repository, clock):
        self._repository = repository
        self._clock = clock

    def create_task(self, project_id, name, description):
        project = self._repository.read_project(project_id)

        task = project.create_task(
            name=name, description=description
        )
        return self._repository.save(task)

    def _read_task(self, task_id):
        return self._repository.read_task(task_id)

    def update_task(self, task_id, name=NONE, description=NONE):
        task = self._read_task(task_id)

        if name is not NONE:
            task.name = name

        if description is not NONE:
            task.description = description

        self._repository.save(task)

    def assign_task(self, task_id, user_id):
        task = self._read_task(task_id)

        task.assign_to(user_id)

        self._repository.save(task)


    def start_task(self, task_id):
        task = self._read_task(task_id)

        task.start(self._clock.now())

        self._repository.save(task)

    def resolve_task(self, task_id, resolution):
        task = self._read_task(task_id)

        task.resolve(self._clock.now(), resolution)

        self._repository.save(task)

    def close_task(self, task_id):
        task = self._read_task(task_id)

        task.close(self._clock.now())

        self._repository.save(task)

    def comment_task(self, task_id, user_id, message):
        task = self._read_task(task_id)

        comment = task.comment(user_id, message)

        return self._repository.save(comment)

    def list_task_comments(self, task_id):
        return self._repository.list_comments(task_id=task_id)

    def list_project_tasks(self, project_id):
        return self._repository.list_tasks(project_id=project_id)


class DjangoRepository(object):
    def save(self, object):
        object.save()

        return object.pk

    def read_project(self, project_id):
        return Project.objects.get(pk=project_id)

    def read_task(self, task_id):
        return Task.objects.get(pk=task_id)

    def tasks_by_ids(self, tasks):
        return Task.objects.filter(pk__in=tasks)

    def list_projects(self):
        return Project.objects.all()

    def list_comments(self, task_id=None):
        query = Comment.objects

        if task_id is not None:
            query = query.filter(task_id=task_id)

        return query.all()

    def list_tasks(self, project_id=None):
        query = Task.objects

        if project_id is not None:
            query = query.filter(project_id=project_id)

        return query.all()