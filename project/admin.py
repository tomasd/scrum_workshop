from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import User
from project.models import Project, Task, Sprint, Comment

admin.site.register(Project)
admin.site.register(Task)
admin.site.register(Sprint)
admin.site.register(Comment)