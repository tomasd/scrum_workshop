from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Project(models.Model):
    shortcut = models.CharField(max_length=3)
    name = models.CharField(max_length=30)

    def change_name(self, name):
        self.name = name

    def change_shortcut(self, shortcut):
        self.shortcut = shortcut

    def plan_sprint(self, start, end, tasks):
        sprint = Sprint(project=self, start=start, end=end)

        for task in tasks:
            task.assign_to_sprint(sprint)

        return sprint

    def create_task(self, name, description):
        task = Task(
            project=self, name=name, description=description
        )

        return task

    def __unicode__(self):
        return self.name


class Sprint(models.Model):
    project = models.ForeignKey(Project)
    start = models.DateField()
    end = models.DateField()


class Task(models.Model):
    project = models.ForeignKey(Project)
    sprint = models.ForeignKey(Sprint, null=True)

    name = models.CharField(max_length=100)
    description = models.TextField(max_length=10000, null=True)

    assigned_user = models.ForeignKey(User, null=True)
    started = models.DateTimeField(null=True)
    resolved = models.DateTimeField(null=True)
    closed = models.DateTimeField(null=True)

    resolution = models.TextField(null=True)

    def assign_to(self, user_id):
        self.assigned_user_id = user_id

    def start(self, date):
        if not self.started:
            self.started = date

    def resolve(self, date, resolution):
        if not self.resolved:
            self.resolved = date
            self.resolution = resolution

    def close(self, date):
        if not self.closed:
            self.closed = date

    def comment(self, author_id, message):
        return Comment(author_id=author_id, message=message, task=self)

    def assign_to_sprint(self, sprint):
        if not self.sprint:
            self.sprint = sprint

    @property
    def is_started(self):
        return not self.is_resolved and self.started is not None

    @property
    def is_resolved(self):
        return not self.is_closed and self.resolved is not None

    @property
    def is_closed(self):
        return self.closed is not None



class Comment(models.Model):
    author = models.ForeignKey(User)
    task = models.ForeignKey(Task)
    message = models.TextField()

    @property
    def author_name(self):
        return self.author.get_full_name() or self.author.username