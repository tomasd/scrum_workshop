from datetime import date, datetime
from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.
from project.application import ProjectService, DjangoRepository, TaskService
from project.models import Project, Task, Comment, Sprint


class ProjectServiceTest(TestCase):
    def setUp(self):
        self.service = ProjectService(repository=DjangoRepository())

    def test_create_project(self):
        project_id = self.service.create_project(
            shortcut='MPR', name='My project'
        )

        self.assertIsNotNone(Project.objects.get(
            id=project_id, shortcut='MPR', name='My project'
        ))

    def test_update_project(self):
        project_id = self.service.create_project(
            shortcut='MPR', name='My project'
        )

        self.service.update_project(project_id, shortcut='CHG', name='Changed')

        self.assertIsNotNone(Project.objects.get(
            id=project_id, shortcut='CHG', name='Changed'
        ))

    def test_plan_sprint(self):
        project_id = self.service.create_project(
            shortcut='MPR', name='My project'
        )

        task = Task(project_id=project_id, name='a', description='b')
        task.save()

        sprint_id = self.service.plan_sprint(project_id, start=date(2010,1,1), end=date(2010,1, 14), tasks=[task.pk])

        self.assertIsNotNone(Sprint.objects.get(pk=sprint_id))


class TaskServiceTest(TestCase):
    def setUp(self):
        self.service = TaskService(repository=DjangoRepository(), clock=datetime)
        self.project = Project(shortcut='MPR', name='My project')
        self.project.save()

        self.project_id = self.project.pk

    def test_create_task(self):
        task_id = self.service.create_task(
            self.project_id, name='Do something', description='Do it like this'
        )

        self.assertIsNotNone(
            Task.objects.get(
                pk=task_id, name='Do something', description='Do it like this'
            )
        )

class TaskChangeServiceTest(TaskServiceTest):
    def setUp(self):
        super(TaskChangeServiceTest, self).setUp()
        task = Task(project=self.project, name='Do something',
                    description='Do it like this')
        task.save()
        self.task_id = task.pk

        user = User()
        user.save()

        self.user_id = user.pk

    def test_update_task(self):
        self.service.update_task(self.project_id, name='Changed name', description='Changed')

        self.assertIsNotNone(Task.objects.get(pk=self.task_id, name='Changed name', description='Changed'))

    def test_assign_task(self):
        self.service.assign_task(self.task_id, self.user_id)

        self.assertIsNotNone(Task.objects.get(pk=self.task_id, assigned_user__pk=self.user_id))

    def test_start_task(self):
        self.service.start_task(self.task_id)

        task = Task.objects.get(pk=self.task_id, started__isnull=False)
        self.assertIsNotNone(task)
        self.assertTrue(task.is_started)
        self.assertFalse(task.is_resolved)
        self.assertFalse(task.is_closed)


    def test_resolve_task(self):
        self.service.resolve_task(self.task_id, "I'm resolving it")

        task = Task.objects.get(pk=self.task_id, resolved__isnull=False,
                                resolution="I'm resolving it")
        self.assertIsNotNone(task)
        self.assertFalse(task.is_started)
        self.assertTrue(task.is_resolved)
        self.assertFalse(task.is_closed)

    def test_close_task(self):
        self.service.close_task(self.task_id)

        task = Task.objects.get(pk=self.task_id, closed__isnull=False)
        self.assertIsNotNone(task)
        self.assertFalse(task.is_started)
        self.assertFalse(task.is_resolved)
        self.assertTrue(task.is_closed)

    def test_comment_task(self):
        comment_id = self.service.comment_task(self.task_id, self.user_id, "I'm commenting it")

        self.assertIsNotNone(Comment.objects.get(pk=comment_id, author__pk=self.user_id, message="I'm commenting it"))

