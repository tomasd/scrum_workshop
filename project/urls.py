from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
   url(r'project/create$', 'project.views.create_project', name='project.create'),
   url(r'project/(\d+)$', 'project.views.project_detail', name='project.detail'),
   url(r'project/$', 'project.views.project_list', name='project.list'),

   url(r'project/(\d+)/create-task$', 'project.views.task_create', name='task.create'),
   url(r'task/(\d+)$', 'project.views.task_detail', name='task.detail'),
   url(r'task/(\d+)/assign-user$', 'project.views.assign_user', name='task.assign_user'),
   url(r'task/(\d+)/start', 'project.views.start_task', name='task.start'),
   url(r'task/(\d+)/resolve', 'project.views.resolve_task', name='task.resolve'),
   url(r'task/(\d+)/close', 'project.views.close_task', name='task.close'),
   url(r'task/(\d+)/comment', 'project.views.comment_task', name='task.comment'),
   url(r'project/(\d+)/tasks/$', 'project.views.task_list', name='task.list'),
)